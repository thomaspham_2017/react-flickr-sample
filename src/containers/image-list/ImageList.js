import React, { Component } from 'react';
import { connect } from 'react-redux';
import Lightbox from 'react-images';
import './ImageList.css';

class ImageList extends Component {
    constructor () {
        super();

        this.state = {
            lightboxIsOpen: false,
            currentImage: 0,
        };

        this.closeLightbox = this.closeLightbox.bind(this);
        this.gotoNext = this.gotoNext.bind(this);
        this.gotoPrevious = this.gotoPrevious.bind(this);
        this.gotoImage = this.gotoImage.bind(this);
        this.handleClickImage = this.handleClickImage.bind(this);
        this.openLightbox = this.openLightbox.bind(this);
        this.images = [];
    }
    openLightbox (index, event) {
        event.preventDefault();
        this.setState({
            currentImage: index,
            lightboxIsOpen: true,
        });
    }
    closeLightbox () {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false,
        });
    }
    gotoPrevious () {
        this.setState({
            currentImage: this.state.currentImage - 1,
        });
    }
    gotoNext () {
        this.setState({
            currentImage: this.state.currentImage + 1,
        });
    }
    gotoImage (index) {
        this.setState({
            currentImage: index,
        });
    }
    handleClickImage () {
        if (this.state.currentImage === this.images.length - 1) return;

        this.gotoNext();
    }
    renderList(){
        this.images = [];
        if (!this.props.items) return;

        const gallery =  this.props.items.map( (item,index) => {
            let highres_img = item.media.m.replace('m.','b.');
            let titleShort = item.title.length < 50 ? item.title : item.title.substr(0, 50) + "..."
            let title = item.title;
            let tags = item.tags.length < 50 ?  item.tags : item.tags.substr(0, 50) + "...";
            let author = item.author;
            let image = {
                src : item.media.m,
                srcSet: [
                    highres_img
                ],
                caption : title,
                thumbnail : item.media.m
            }
            this.images.push(image);

            return (

                <div className="col-md-3" key={index} onClick={(e) => this.openLightbox(index, e)}>
                    <div className="flickr-img">
                        <div className="text">
                            <h6 className="text-title"><b>{title}</b></h6>
                        </div>
                        <a href={highres_img} title={titleShort} target="_blank"><img className="img-responsive" src={item.media.m} alt={title} title={title}/></a>
                        <div className="text">
                            <h6 className="text-author">Author: <b>{author}</b></h6>
                            <h6 className="text-tags">Tags: <b>{tags}</b></h6>
                        </div>
                    </div>
                </div>
            );
        });

        return (
            <div className="row">
                {gallery}
            </div>
        );
    }

    render(){
        if(this.props.items && this.props.items != null){
            return (
                <div className="section">
                    {this.props.heading && <h2>{this.props.heading}</h2>}
                    {this.props.subheading && <p>{this.props.subheading}</p>}
                    {this.renderList()}
                    <Lightbox
                        currentImage={this.state.currentImage}
                        images={this.images}
                        isOpen={this.state.lightboxIsOpen}
                        onClickImage={this.handleClickImage}
                        onClickNext={this.gotoNext}
                        onClickPrev={this.gotoPrevious}
                        onClickThumbnail={this.gotoImage}
                        onClose={this.closeLightbox}
                        preventScroll={this.props.preventScroll}
                        showThumbnails={this.props.showThumbnails}
                        spinner={this.props.spinner}
                        spinnerColor={this.props.spinnerColor}
                        spinnerSize={this.props.spinnerSize}
                        theme={this.props.theme}
                    />
                </div>
            );
        }

        return <div></div>;
    }

}

function mapStateToProps(state){
    return {
        items : state.items
    };
}

export default connect(mapStateToProps)(ImageList);
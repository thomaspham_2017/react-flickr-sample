import React, {Component} from "react";
import {connect} from 'react-redux';
import {fetchImage} from '../../actions/index';
import {bindActionCreators} from 'redux';
import './SearchBar.css'

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {searchTerm: ''};
        this.handleSearchTermChange = this.handleSearchTermChange.bind(this);
    }

    render() {
        return (
            <div className="input-group search-container">
                <input value={this.state.searchTerm} placeholder="Enter search term here" className="form-control"
                       onChange={this.handleSearchTermChange} onKeyPress={this.onKeyPress.bind(this)} type="text"/>
                <span className="input-group-btn">
                    <button onClick={() => this.props.fetchImage(this.state.searchTerm)}
                            className="btn btn-primary">Search</button>
                    </span>
            </div>
        );
    }

    handleSearchTermChange(event) {
        this.setState({searchTerm: event.target.value});
    }

    onKeyPress(e) {
        if (e.key === 'Enter') {
            this.props.fetchImage(this.state.searchTerm);
        }
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchImage}, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);

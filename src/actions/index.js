import axios from 'axios';
import {API_URL, FETCH_IMAGE_DATA} from "../constants/contants";

export function fetchImage(searchTerm){
    const url =   API_URL + searchTerm;
    const response = axios.get(url);
    return {
        type : FETCH_IMAGE_DATA,
        payload: response
    }
}

export default FETCH_IMAGE_DATA;
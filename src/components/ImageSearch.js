import React from "react";
import SearchBar from "../containers/search-bar/SearchBar";
import ImageList from "../containers/image-list/ImageList";

const ImageSearch = ()=> {
    return (
        <div className="container">
            <SearchBar />
            <ImageList/>
        </div>
    );
}

export default ImageSearch;

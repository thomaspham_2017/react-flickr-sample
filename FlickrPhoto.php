<?php
header("Content-Type:application/json");
$tag = '';
if(isset($_GET["tags"]))
{
    $tag = $_GET["tags"];
}
$response = file_get_contents('https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1&tags='. $tag);
echo $response;
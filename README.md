# react-flickr-sample


### Tech

* [React] - A JavaScript library for building user interfaces!
* [Redux Framework] - Redux is a predictable state container for JavaScript apps.
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [ES6] - ES6 brings many new feature like concept of classes, template tags, arrow functions etc.
* [PHP] - Server side call Flickr API to resolve 'Same-origin policy' problem

### Installation

The app requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd react-flickr-sample
$ npm install
```
### How to run

```sh
$ cd react-flickr-sample
$ npm start
```

### Live Demo

https://warm-stream-57617.herokuapp.com/
